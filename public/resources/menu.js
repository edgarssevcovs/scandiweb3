a = document.getElementById('menu');
btn = document.getElementById('btn');
form = document.getElementById('action');
a.addEventListener('change', function () {
var selected = a.value;
    switch(selected) {
        case 'add' :
            btn.name = "add-product";
            form.action = '/add';
            break;
        case 'delete' :
            btn.name = "delete-btn";
            form.action = 'index/deleteProduct';
            break;
        default:
            btn.name = "add-product";
            form.action = '/add';
    }
});