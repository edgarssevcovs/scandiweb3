var type = document.getElementById('type');

type.addEventListener('change', function() {
    a = document.querySelector('.size-input-row');
    b = document.querySelector('.weight-input-row');
    c = document.querySelector('.dimension-input-row');
    size = document.getElementById('size');
    weight = document.getElementById('weight');
    height = document.getElementById('height');
    width = document.getElementById('width');
    length = document.getElementById('length');

    a.style.display = "none";
    b.style.display = "none";
    c.style.display = "none";
    var selected = type.value;

    switch(selected) {
        case 'disc' :
            a.style.display = 'inline';
            size.required = true;
            weight.required = false;
            height.required = false;
            width.required = false;
            length.required = false;
            break;
        case 'book' :
            b.style.display = 'inline';
            weight.required = true;
            size.required = false;
            height.required = false;
            width.required = false;
            length.required = false;
            break;
        case 'furniture':
            c.style.display = 'inline';
            weight.required = false;
            size.required = false;
            height.required = true;
            width.required = true;
            length.required = true;
            break;
        default:
            document.querySelector('.special-attribute').innerHTML = "";
    }
});   