<?php

class indexController extends Controller {

    public function index() {
    $data['products'] = $this->model('index')->getAllProducts();

        $this->view('index/header');
        $this->view('index/products', $data);
        $this->view('index/footer');
    }

    public function deleteProduct() {
        if(isset($_POST['checkbox'])) {
            $ids = implode(',', $_POST['checkbox']);
            $this->model('index')->delete($ids);
            header('Location: /public/');
        }
    }
}