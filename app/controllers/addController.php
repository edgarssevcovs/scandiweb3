<?php

class AddController extends Controller {

    public $sku;
    public $name;
    public $price;
    public $type;
    public $attr;
    public $prodAttr;

    public function __construct() {
        if(isset($_POST['sku'])) {
            $this->sku = $_POST['sku'];
            $this->name = $_POST['name'];
            $this->price = $_POST['price'];
            $this->type = $_POST['type'];
            $this->attr = $_POST['attr'];
        }
    }
    
    public function index() {
        $this->view('add/add');
    }

    public function getValues() {
        $prodType = ucwords($this->type);
        $new = new $prodType;
        $new->setAttr($this->attr);
    }

    public function new() {
        $this->model('add')->insert($this->sku, $this->name, $this->price, $this->type, $this->prodAttr);
        header('Location: /public/');
    }
}

class Disc extends AddController {
    public function setAttr($attr) {
        $this->prodAttr = 'Size: ' . $attr[0] . ' MB';
        $this->new();
    }
}

class Book extends AddController {
    public function setAttr($attr) {
        $this->prodAttr = 'Weight: ' . $attr[1] . ' Kg';
        $this->new();
    }
}

class Furniture extends AddController {
    public function setAttr($attr) {
        $this->prodAttr = 'Dimensions: ' . $attr[2] . 'x' . $attr[3] . 'x' . $attr[4];
        $this->new();
    }
}
