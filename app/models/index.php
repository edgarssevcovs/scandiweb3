<?php

class Index {

    private $connection;

    public function __construct() {
        $this->connection = new Connection;
    }

    public function getAllProducts() {
        $this->connection->queryPrepare('SELECT * FROM products');
        return $this->connection->getAll();
    }

    public function delete($ids) {
        $this->connection->queryPrepare("DELETE FROM products WHERE id in ($ids)");
        return $this->connection->exec();
        $this->redirect('index');
    }

}