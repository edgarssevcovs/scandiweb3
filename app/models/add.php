<?php

class Add {

    private $connection;

    public function __construct() {
        $this->connection = new Connection;
    }

    public function insert($sku, $name, $price, $type, $attribute) {
        $this->connection->queryPrepare("INSERT INTO products (sku, name, price, type, attribute) VALUES ('$sku', '$name', '$price', '$type', '$attribute')");
        $this->connection->paramBind('sku', $sku);
        $this->connection->paramBind('name', $name);
        $this->connection->paramBind('price', $price);
        $this->connection->paramBind('type', $type);
        $this->connection->paramBind('attribute', $attribute);

        return $this->connection->exec();
        header('/public');
    }
}