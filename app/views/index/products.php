<?php

foreach($data['products'] as $key => $value): ?>
    <div class = 'item'>
        <input type = 'checkbox' name = 'checkbox[]' value = "<?= $value['id'] ?>">
        <div class = 'item-description'>
            <?= $value['sku']; ?>
        </div>
        <div class = 'item-description'>
            <?= $value['name']; ?>
        </div>
        <div class = 'item-description'>
            <?= $value['price']; ?> &euro;
        </div>
        <div class = 'item-description'>
            <?= $value['attribute']; ?>
        </div>
    </div>
<?php endforeach; ?>