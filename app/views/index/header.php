<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="/public/resources/style.css">
    <title>Scandiweb</title>
</head>
<body>
    <div class="content">
        <div class="top">
            <div class="top-left">
                Product List
            </div>
            <div class="top-right">
                <div class = "menu">
                    <select name = "menu" id = "menu">
                        <option value = "add">Add Product</option>
                        <option value = "delete">Mass Delete</option>
                    </select>
                </div>
                <div class = "menu">
                    <form action = "add" method = "POST" id = "action">
                        <button type = "submit"  class = "btn-green" name = "add-product" id = "btn">APPLY</button>
                </div>
                <script src = "/public/resources/menu.js"></script>
            </div>
        </div>
        <div class="products">