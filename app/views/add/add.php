<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="/public/resources/style.css">
    <title>Scandiweb | Add Product</title>
</head>
<body>
    <div class="content">
        <div class="top">
            <div class="top-left">
                Product Add
            </div>
            <div class="top-right">
                <div class = "menu">
                    <button class = "btn-red" onclick="window.location.href='index'">Back</button>
                </div>
                <div class = "menu">
                    <form action = "/public/add/getValues" method = "POST">
                    <button type = "submit" name = "add-btn" class = "btn-green">Save</button>
                </div>
            </div>  
        </div>
        <div class="products">
                <div class="input-row">
                   SKU 
                   <input type = "text" name = "sku" required>
                </div>
                <div class="input-row">
                    Name 
                    <input type = "text" name = "name" required>
                </div>
                <div class="input-row">
                    Price 
                    <input type = "number" name = "price" required>
                </div>
                <div class="input-row">
                    Type Switcher
                    <select name = "type" id = "type" required>
                        <option value = "">Type Switcher</option>
                        <option value = "disc">DVD-disc</option>
                        <option value = "book">Book</option>
                        <option value = "furniture">Furniture</option>
                    </select>
                </div>
                <div class="special-attribute">
                    <div class="size-input-row">
                        <div class = "input-description">
                            Please enter disc size in megabytes. e.g. if capacity is 700MB, enter "700" in input box.
                        </div>
                        <div class = "input-row">
                            Size <input type = "number" name = "attr[0]" id = "size">
                        </div>
                    </div>
                    <div class="weight-input-row">
                        <div class = "input-description">
                            Please enter weight in kilograms. e.g. if weight is 500g, enter "0.5" in input box.
                        </div>
                        <div class = "input-row">
                            Weight <input type = "number" name = "attr[1]" step = "0.1" id = "weight">
                        </div>
                    </div>  
                    <div class="dimension-input-row">
                        <div class = "input-description">
                            Please enter dimensions in following format - HxWxL in centimeters.
                        </div>
                        <div class = "input-row">
                            Height <input type = "number" name = "attr[2]" id = "height">
                        </div>
                        <div class = "input-row">
                            Width <input type = "number" name = "attr[3]" id = "width">
                        </div>
                        <div class = "input-row">
                            Length <input type = "number" name = "attr[4]" id = "length">
                        </div>
                    </div>
                </div>  
            </form>
        </div>
    </div>
<script src = "/public/resources/attribute.js"></script>
</body>
</html>