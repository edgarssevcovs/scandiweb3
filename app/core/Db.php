<?php

class Connection {

    private $hostname = '127.0.0.1';
    private $username = 'root';
    private $password = '';
    private $database = 'scandiweb';
    private $conn;
    private $stmt;

    public function __construct() {
        $dbh = 'mysql:host=' . $this->hostname . ';dbname=' . $this->database;
        $attr = [PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION];

        try {
            $this->conn = new PDO($dbh, $this->username, $this->password, $attr);
        } catch(PDOException $e) {
            die($e->getMessage());
        }
    }

    public function queryPrepare($query) {
        $this->stmt = $this->conn->prepare($query);
    }

    public function paramBind($param, $value, $type = null) {
        if(is_null($type)) {
            switch(true) {
                case is_int($value):
                    $type = PDO::PARAM_INT;
                    break;
                case is_null($value):
                    $type = PDO::PARAM_STR;
                    break;
                default:
                    $type = PDO::PARAM_STR;
            }
        }
        return $this->stmt->bindParam($param, $value, $type);
    }

    public function exec() {
        $this->stmt->execute();
    }

    public function getAll() {
        $this->exec();
        return $this->stmt->fetchAll(PDO::FETCH_ASSOC);
    }
}