<?php

class App {

    protected $controller = 'indexController.php';
    protected $method = 'index';
    protected $params = [];

    public function __construct() {
        $url = $this->parseURL();

        if(file_exists('../app/controllers/'.$url[0].'.php')) {
            $this->controller = $url[0];
            unset($url[0]);
        } else {
            echo "Not Found {$url[0]}";
        }

        require_once '../app/controllers/'.$this->controller.'.php';
        $this->controller = new $this->controller;

        if(isset($url[1])) {
            if(method_exists($this->controller, $url[1])) {
                $this->method = $url[1];
                unset($url[1]);
            }
        }

        if(!empty($url)) {
            $this->params = array_values($url);
        }
    }

    public function parseURL() {
        if(isset($_GET['url'])) {
            $url = explode('/', filter_var(trim($_GET['url']), FILTER_SANITIZE_URL));
            $url[0] = $url[0] . 'Controller';
        } else {
            $url[0] = 'indexController';
        }
        return $url;
    }

    public function run() {
        return call_user_func_array([$this->controller, $this->method], $this->params);
    }
}